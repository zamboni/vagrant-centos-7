# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'yaml'

local_settings = YAML.load_file 'local_settings.yml'


Vagrant.configure(2) do |config|
    config.ssh.insert_key = false
    config.vm.box = "bento/centos-7.1"

    config.vm.provider "virtualbox" do |v|
      v.memory = 1024
      v.cpus = 2
    end

    # Network
    config.vm.network "forwarded_port", guest: 80, host: 18080
    config.vm.network "forwarded_port", guest: 8000, host: 18000
    config.vm.network "forwarded_port", guest: 5432, host: 18500
    if local_settings.key?('forwarded_ports')
        local_settings['forwarded_ports'].each do |forwarded_port|
            config.vm.network "forwarded_port", guest: forwarded_port['guest'], host: forwarded_port['host']
        end
    end

    # Synced folders
    if local_settings.key?('synced_folders')
        local_settings['synced_folders'].each do |synced_folder|
            config.vm.synced_folder synced_folder['host'], synced_folder['guest']
        end
    end

    # Provisioning
    config.vm.provision "ansible_local" do |ansible|
        ansible.playbook = "ansible/playbook.yml"
        ansible.install_mode = "pip"
    end
end
