#!/usr/bin/env bash

# bash_prompt
su vagrant << EOF
if cat ~/.bashrc | grep ".bash_prompt"
	then echo ".bash_prompt already in bashrc"
	else cp /vagrant/.bash_prompt ~/.bash_prompt && echo ". ~/.bash_prompt" >> ~/.bashrc
fi
EOF
if cat ~/.bashrc | grep ".bash_prompt"
	then echo ".bash_prompt already in bashrc"
	else cp /vagrant/.bash_prompt ~/.bash_prompt && echo ". ~/.bash_prompt" >> ~/.bashrc
fi

sudo yum install -y epel-release

# utils
yum install -y htop vim

# requirements
sudo yum install -y gcc autoconf flex bison libjpeg-turbo-devel
sudo yum install -y freetype-devel zlib-devel zeromq-devel gdbm-devel ncurses-devel
sudo yum install -y automake libtool libffi-devel curl git tmux
sudo yum install -y libxml2-devel libxslt-devel
sudo yum install -y wget openssl-devel gcc-c++

# nginx
sudo yum install -y nginx

# private certs
cd /etc/nginx && mkdir certs;
openssl genrsa -des3 -passout pass:x -out vagrant_server.pass.key 2048
openssl rsa -passin pass:x -in vagrant_server.pass.key -out vagrant_server.key
rm vagrant_server.pass.key
openssl req -new -key vagrant_server.key -out vagrant_server.csr \
  -subj "/C=IT/ST=Italy/L=Udine/O=Vagrant/OU=Devs/CN=vagrantVM"
openssl x509 -req -days 365 -in vagrant_server.csr -signkey vagrant_server.key -out vagrant_server.crt

sudo systemctl start nginx
sudo systemctl enable nginx


# postgresql
mkdir -p tmp && cd tmp;
wget http://yum.postgresql.org/9.5/redhat/rhel-7-x86_64/pgdg-centos95-9.5-2.noarch.rpm
rpm -ivh pgdg-centos95-9.5-2.noarch.rpm
sudo yum install -y postgresql95-server postgresql95-devel postgresql95-contrib
export PATH=$PATH:/usr/pgsql-9.5/bin
postgresql95-setup initdb
sudo systemctl enable postgresql-9.5.service
sudo sed -i 's/ident/md5/' /var/lib/pgsql/9.5/data/pg_hba.conf
sudo systemctl start postgresql-9.5.service

# Dashboard requirements
yum install -y python-devel
sudo easy_install pip
sudo pip install virtualenvwrapper

# envs and repositories
sudo yum install -y mercurial
su vagrant << EOFV
cd /home/vagrant;
mkdir -p repositories
mkdir -p envs
export WORKON_HOME=~/envs
source /bin/virtualenvwrapper.sh
cat > ~/.bash_virtualenvwrapper << EOF
export WORKON_HOME=~/envs
source /bin/virtualenvwrapper.sh
EOF
if cat ~/.bashrc | grep "virtualenvwrapper"
	then echo "virtualenvwrapper already in bashrc"
	else echo ". ~/.bash_virtualenvwrapper" >> ~/.bashrc
fi
EOFV
