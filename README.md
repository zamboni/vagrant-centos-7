# vagrant-centos-7

This repository constains Ansible playbooks to create a Vagrant VM.
All credentials must be changed manually after the machine is created.

## Packages installed

- Postgres 9.5
- Redis 
- Elasticsearch
- Python and Virtualenvwrapper
- Circus