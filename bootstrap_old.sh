#!/usr/bin/env bash

cd /etc/nginx && mkdir certs;
openssl genrsa -des3 -passout pass:x -out vagrant_server.pass.key 2048
openssl rsa -passin pass:x -in vagrant_server.pass.key -out vagrant_server.key
rm vagrant_server.pass.key
openssl req -new -key vagrant_server.key -out vagrant_server.csr \
  -subj "/C=IT/ST=Italy/L=Udine/O=Vagrant/OU=Devs/CN=vagrantVM"
openssl x509 -req -days 365 -in vagrant_server.csr -signkey vagrant_server.key -out vagrant_server.crt

 